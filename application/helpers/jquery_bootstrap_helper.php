<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('format_input'))
{
    /**
     * this will create an input form group
     * @param $id string
     * @param $size string (lg, md, xs, sm)
     * @param $attr string 
     * @return string 
    **/
    function format_input($id, $label, $size='xs', $type='text', $attr='', $class="form-control", $placeholder=''){
        $str = '<div class="input-group input-group-'.$size.'">
				  <span class="input-group-addon">'.$label.'</span>
				  <input type="'.$type.'" id="'.$id.'" name="'.$id.'" placeholder="'.$placeholder.'" class="'.$class.'" aria-describedby="'.$id.'" '.$attr.'>
				</div>';
        echo $str;
    }
}

if(!function_exists('format_select'))
{
    function format_select($id, $label, $data, $default_value = false, $attr='', $size='xs', $default = true, $class="form-control"){
        $str = '<div class="input-group input-group-'.$size.'">
                  <span class="input-group-addon" >'.$label.'</span>
                  <select class="'.$class.'" id="'.$id.'" name="'.$id.'" '.$attr.'>';
            if($default){
                $str .= '<option value="">Select Value</option>';
            }
            foreach($data as $d){
                if($default_value == $d['val']){
                    $str .= '<option value="'.$d['val'].'" selected>'.$d['desc'].'</option>';
                }else{
                    $str .= '<option value="'.$d['val'].'">'.$d['desc'].'</option>';
                }
                
            }
        $str .= ' </select>
				</div>';
        echo $str;
    }
}