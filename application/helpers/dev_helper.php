<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
    if(!function_exists('pd'))
    {
            function pd($d='')
            {
                    echo '<pre>';
                            print_r($d);
                    echo '</pre>';
                    die();
            }
    }
     
    if(!function_exists('vd'))
    {
            function vd($d='')
            {
                    echo '<pre>';
                            var_dump($d);
                    echo '</pre>';
                    die();
            }
    }

    /**
     * uses util.php library
     */
    if(!function_exists('ud'))
    {
            function ud($d='')
            {
                    echo '<pre>';
                            util::var_dump($d);
                    echo '</pre>';
                    die();
            }
    }

    /**
     * uses util.php library
     */
    if(!function_exists('up'))
    {
            function up($d='')
            {
                    echo '<pre>';
                            util::var_dump($d);
                    echo '</pre>';
            }
    }
     
    if(!function_exists('pp'))
    {
            function pp($d='')
            {
                    echo '<pre>';
                            print_r($d);
                    echo '</pre>';
            }
    }
     
    if(!function_exists('vp'))
    {
            function vp($d='', $clr = false)
            {
					if($clr){
						echo '<div class="well"></div>';
					}
                    echo '<pre>';
                            var_dump($d);
                    echo '</pre>';
            }
    }

    if(!function_exists('object_to_view_data'))
    {
        function object_to_view_data($bato)
        {
            $CI=& get_instance();
            
            if($bato){
                foreach ($bato as $key => $value) {
                    $CI->view_data[$key] = $value;
                }
            }
        }
    }

    if(!function_exists('iset'))
    {
        function iset($value)
        {
            return isset($value)?$value:"";
        }
    }

    /**
     * Convert string to lower and UCwords
     */
    if(!function_exists('uc_words'))
    {
        function uc_words($value)
        {
            return ucwords(strtolower($value));
        }
    }

    if(!function_exists('convert_to_mysql_date'))
    {
        function convert_to_mysql_date($date)
        {
            //get delimeter
            $delimeter = "-";
            if (strpos($date,'/') !== false) {
                $delimeter = "/";
            }

            $r = explode($delimeter,$date);

            if(count($r) == 3){
                //mm-dd-yy
                return $r[2].'-'.$r[0].'-'.$r[1];    
            }else{
                return "1970-01-01";
            }
        }
    }

    /**
     * Dump last Query
     */
    if(!function_exists('lq'))
    {
        function lq()
        {
            $CI=& get_instance();
            pd($CI->db->last_query());
        }
    }


    if(!function_exists('clean'))
    {
        /**
         * this will remove trailing spaces and apply UC First in the string
         * @param $str string
        **/
        function clean($str){
            if(isset($str)) return $str;
            
            $str = ucwords(strtolower(trim($str)));
            return $str;
        }
    }

    if(!function_exists('ds'))
    {
        /**
         * This will dump the content of the session
         * @param $str string
        **/
        function ds(){

            $CI=& get_instance();
            vd($CI->session);
        }
    }

