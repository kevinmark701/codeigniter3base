<?php
function _encrypt($string){

    $myconfig =&get_config(); 
    $key = $myconfig['encryption_key'];
    $hash = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
    return $hash;
}

function _decrypt($string){

    $myconfig =&get_config(); 
    $key = $myconfig['encryption_key'];
    $xstring = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
    return $xstring;
}