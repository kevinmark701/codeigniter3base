<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * CSV Helpers
 * 
 * 
 * @author		Kevin Mark A. Pecdasen
 */

/**
	    * @param array $headings - to be placed at the top of the excell
	    * @param array $tableHeader - the table header titles
	    * @param array $order - oder of data for output
	    * @param array(object) $data - the contents
*/
if ( ! function_exists('generateExcel'))
{	
	function generateExcel($headings = false, $tableHeader, $order, $data,$title,$column_width = false)
	{
		/* sets the default column width*/
		$column_width = $column_width === false ? 30 : $column_width;
		
		$ci =& get_instance();
    	$ci->load->library('excel');
    	$objsheet = $ci->excel->getActiveSheet();

    	ini_set('memory_limit','500M');
		ini_set('max_execution_time',100);
		$c = 0; //the counter

		/*Set cell width*/
		$w = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',);

		/*Title*/
		$objsheet->setTitle($title);
		/*sets the title and the headings of the document*/
		for ($i=0; $i < count($headings); $i++) { 
			$objsheet->setCellValue($w[0].($c+1),$headings[$i]);
			/*sets to center*/
			$objsheet->getStyle($w[0].($c+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/*merge the cells*/
			$objsheet->mergeCells($w[0].($c+1).':'.$w[count($tableHeader)-1].($c+1));
			$c++;

			/*sets the width of the columns*/
			$objsheet->getColumnDimension($w[$i])->setWidth($column_width);
		}

		/*adds a space between the content and the headings*/
		$c++;
		$c++;

		/*prints the table headers*/
		for ($i=0; $i < count($tableHeader); $i++) { 
			$objsheet->setCellValue($w[$i].$c, $tableHeader[$i]);
			// $objsheet->getColumnDimension($w[$i].$c, $tableHeader[$i])->setAutoSize(true);
		}
		/*adds another space*/
		$c++;

		$count_h = 0; //count horizontal
		/*prints the contents*/
		foreach ($data as $k => $v) {
			/*prints horizontaly*/
			foreach ($order as $kk => $vv) {
				$objsheet->setCellValue($w[$count_h].$c, $v->$order[$kk]);
				// $objsheet->getColumnDimension($w[$count_h].$c, $v->$order[$kk])->setAutoSize(true);
				$count_h++;
			}
			$count_h = 0;
			$c++;
		}
		

		/*finalization*/
		$fn = $title.date('m_d_y_h-i-s',time()).'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachement; filename = "' . $fn . '"');
		header('Cache-Control: max-age=0');
		$objWri = PHPExcel_IOFactory::createWriter($ci->excel, 'Excel5');
		@ob_end_clean();
		$objWri->save('php://output');
		@$objsheet->disconnectWorksheets();
		unset($objectSheet);
		exit(0);
	}
}