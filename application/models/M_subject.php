<?php
class M_subject Extends MY_Model
{
	
	protected $_table = 'subjects';
	public function __construct()
	{
		parent::__construct();
	}

	public function get_user_subjects($sc_id = false)
	{	
		$sql = "select ms.*, s.* from subjects s 
			left join master_subjects ms on s.ms_id = ms.id 
			where s.ay_id = ? and user_id = ? ";
		if($sc_id){
			$sql .= "and sc_id = '".$sc_id."'";
			$res = $this->db->query($sql, [$this->session->userdata('ay')->id, $this->session->userinfo->id])->row();	
		}else{
			$res = $this->db->query($sql, [$this->session->userdata('ay')->id, $this->session->userinfo->id])->result();	
		}
		return count($res) > 0 ? $res : null;
	}

}

?>