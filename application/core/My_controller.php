<?php

class My_Controller Extends CI_Controller
{
	protected $layout_view = 'layout';
	protected $content_view = false;
	protected $disable_layout = FALSE;
	protected $disable_menus = FALSE;
	protected $disable_views = FALSE; //DISABLE THE LOADING OF VIEW - FOR AJAX PURPOSE DEC 11 2013
	protected $view_data = array();

	protected $enable_datatable = true;
	public $title = "";
	public $ay = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// $this->output->enable_profiler(ENVIRONMENT == 'development' ? true : false);

		// $this->user = $this->session->userdata('userType');
		// $this->userlogin = $this->session->userdata('userlogin');
		// $this->userid = $this->session->userdata('userid');
		// $this->islogin = $this->session->userdata("logged_in") ? TRUE : FALSE;

		//check if logged in then redirects to the login page
		// if($this->router->class !== 'login'){
		// 	$this->check_if_logged();
		// 	if($this->title == ""){
		// 		$this->title = ucwords(strtolower(str_replace('_', ' ', $this->router->method )));
		// 	}
		// }
	}
	
	public function set_title($title)
	{
		$this->title = $title;
	}
	
	function pagination_style()
	{
	    // Style
	  $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&lt; Prev';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = 'Next &gt;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      
      return $config;
      
	}


	public function _msg($type = FALSE,$message = FALSE,$redirect = FALSE,$var_name = 'system_message')
	{
		$template = "";
		$type = strtolower(trim($type));
		if($redirect == FALSE){
			$redirect = current_url();
		}

		switch($type)
		{
		 	case $type == 'e':
				$template = "<div id='focusme' class='alert alert-danger'><h4><i class='fa fa-warning'></i> Error!<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></h4> <span style='padding-left:2em'>".$message."</span></div>";
			break;
			case $type == 's':
				$template = "<div id='focusme' class='alert alert-success'><h4><i class='fa fa-check'></i> Success!<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></h4> <span style='padding-left:2em'>".$message."</span></div>";
			break;
			case $type == 'w':
				$template = "<div id='focusme' class='alert alert-warning'><h4><i class='fa fa-warning'></i> Warning!<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></h4> <span style='padding-left:2em'>".$message."</span></div>";		
			break;
			case $type == 'n':
				$template = "<div id='focusme' class='alert alert-info'><i class='fa fa-check'></i>&nbsp; ".$message."<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div>";
			break;
			case $type == 'p':
				$template = $message;
			break;
			case $type === FALSE;
				$template = $message;
			break;
		}
		if($type AND $message AND $redirect)
		{
			$this->session->set_flashdata($var_name,$template);	
			redirect($redirect, 'refresh');

		}elseif($type AND $message AND $redirect == FALSE){
			return $template;
		}
		
		if($message == FALSE AND $redirect == FALSE)
		{
			return $this->session->flashdata($var_name);
		}
	}


	public function _output($output){
		// $this->view_data['user_menus'] = $this->user_menus;
		// $this->view_data['elapsed_time'] = $this->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
		// $this->view_data['memory_usage'] = ( ! function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2).'MB';
	
		if($this->content_view == FALSE && empty($this->content_view)){
			$this->content_view = $this->router->class . '/' . $this->router->method;
		}

		$yield = file_exists(APPPATH . 'views\\' . $this->content_view.'.php') ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE ;

		if($this->disable_layout == FALSE){
			if($this->layout_view)
			{
				$this->view_data['yield'] = $yield;
				if($this->disable_views == false){
					echo $this->load->view('layout/' . $this->layout_view, $this->view_data, TRUE);
				}
			}else{
				if($this->disable_views == false){
					echo $yield;
				}
			}
		}
		else{
			if($this->disable_views == false){
				echo $this->load->view($this->content_view, $this->view_data, TRUE);
			}
		}
		
		if($this->disable_views == false){
			//output profiler
			echo $output;
		}
	}


	public function check_if_logged()
	{
		if(isset($this->session->userdata('userdata')['uname'])){
			if(isset($this->session->userdata('userdata')['is_logged_id']) && $this->session->userdata('userdata')['is_logged_id'] == 1 ){
				if(isset($this->session->userdata('userdata')['role'])){
					#sets the appropriet user data
					$this->get_user_info();
					#loads the menu for the user
					$this->config->load('menus');
					return true;
				}
			}
		}

		$this->_msg('e', "Please log-in first.", base_url('login'));
	}

	public function get_user_info()
	{
		$this->load->model('M_user');
		$info = $this->M_user->pull(['uname' => $this->session->userdata('userdata')['uname'] ]);
		$this->session->set_userdata('userinfo', $info);
	}
}