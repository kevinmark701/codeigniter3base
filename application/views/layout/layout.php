<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="icon" href="images/favicon.ico" type="image/ico" /> -->
  <title></title>
  <link href="<?=asset_url.'vendor/bootstrap3/css/bootstrap.min.css'?>" rel="stylesheet">
</head>
<body>
  <div class="container">
    <h2 class="alert alert-danger">Hi</h2>
    <div class="row">
      <?php
              if($yield){
                echo $yield;
              }
      ?>
    </div>
      
  </div>

  <!-- jQuery -->
  <script src="<?=asset_url.'vendor/jquery/jquery.min.js'?>"></script>
  <!-- Bootstrap -->
  <script src="<?=asset_url.'vendor/bootstrap3/js/bootstrap.min.js'?>"></script>
</body>
</html>
          
          

