function ajax_request(url,data, type = 'post', dataType = 'json'){
    $.ajax({
        url: url,
        type : type,
        dataType : dataType,
        data : data,
        success: function(d){
            return d;
        }
    }); 
}