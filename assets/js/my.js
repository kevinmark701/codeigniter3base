//  Remove System message when x click
$('.sys_m_close').click(function(){
	$(this).parent().parent().hide('slow');
});

//converts everything typed to uppercase
$(".upperCase").on("keyup", function(){
	val = $(this).val().toUpperCase(); 
    $(this).val(val);  
});
 
function filterInput(e, reg){
  if (!e.value.match(reg)) {
    e.value = e.value.slice(0, -1);
  }
}

$('body').on('click','.xconfirm',function(e){
	e.preventDefault();
	var xelement = $(this);
	var url = $(this).attr('href');
	var title = $(this).attr('title') ? $(this).attr('title') : "Are you sure to continue?";
	var click_function = $(this).attr('click_function') != null ? $(this).attr('click_function') : false;
	var is_redirect = $(this).attr('is_redirect') != null && $(this).attr('is_redirect') == 'yes' ? true : false;
	$('#myModal #confirm').unbind();
	$('#myModal').modal({
		'show' : true,
		'backdrop' : 'static',
		'keyboard' : false
	});

   	if(title != "" && title != null){ $('#myModal .modal-body').html('<p>'+title+'</p>'); }

	$('#myModal #confirm').one('click',function(){
	 	if(is_redirect)
	 	{
	 		//if attr is_redirect if fount on the link it will go directy to the link and will not call any function
	 		window.location = url;
	 	}else
	 	{
		 	if(click_function == false){      		
		 		window.location = url;
	    	}else{
	    		//IF attr 'click_function' is found on the link it will call a function which name is value of the click_function attr
	    		window[click_function](url, xelement);
	    		$('#myModal').modal('hide');
	    	}
		}
	});
});
